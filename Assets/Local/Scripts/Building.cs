﻿using UnityEngine;

namespace Assets.Local.Scripts
{
    public class Building: MonoBehaviour
    {
        public Collider ProductionArea;
        public float LastProductionTime;
        public bool IsWork;
    }
}
